/*
 * Copyright 2016 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#define GL_GLEXT_PROTOTYPES

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <EGL/egl.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

static inline uint32_t
minify(uint32_t x, uint32_t level)
{
   return MAX(x >> level, 1);
}

int
main(void)
{
   EGLDisplay *dpy = eglGetDisplay(NULL);

   EGLint egl_major, egl_minor;
   if (!eglInitialize(dpy, &egl_major, &egl_minor))
      abort();

   const EGLint ctx_attribs[] = {
      EGL_CONTEXT_MAJOR_VERSION, 3,
      EGL_CONTEXT_MINOR_VERSION, 0,
      EGL_NONE,
   };

   eglBindAPI(EGL_OPENGL_API);

   EGLContext ctx = eglCreateContext(dpy, NULL, NULL, ctx_attribs);
   if (!ctx)
      abort();

   if (!eglMakeCurrent(dpy, NULL, NULL, ctx))
      abort();

   glClearColor(0.25, 0.50, 0.75, 1.00);

   /* FIXME: Convert env var to cmdline params */
   const char *tex_layout = getenv("DEBUG_TEX_LAYOUT");
   if (!tex_layout)
      abort();

   uint32_t width, height, n_levels;
   if (sscanf(tex_layout, "%x %x %x", &n_levels, &width, &height) != 3)
      abort();

   /* FIXME: Convert env var to cmdline params */
   const char *position = getenv("DEBUG_POSITION");
   if (!position)
      abort();

   uint32_t x, y, level;
   if (sscanf(position, "%x %x %x", &level, &x, &y) != 3)
      abort();

   GLuint tex;
   glGenTextures(1, &tex);
   glBindTexture(GL_TEXTURE_2D, tex);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

   for (int l = 0; l < n_levels; ++l) {
      glTexImage2D(GL_TEXTURE_2D, l, GL_RGBA,
                   minify(width, l), minify(height, l),
                   /*border*/ 0, GL_RGBA,
                   GL_FLOAT, NULL);
   }

   GLuint fb;
   glGenFramebuffers(1, &fb);
   glBindFramebuffer(GL_FRAMEBUFFER, fb);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, tex, level);
   assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

   glClear(GL_COLOR_BUFFER_BIT);

   /* Write a single pixel. */
   glWindowPos2i(x, y);
   glDrawPixels(1, 1, GL_RGBA, GL_FLOAT, (float[]) { 0.33, 0.33, 0.33, 1.00 });
   glFinish();

   /* Forece a resolve from HiZ to Z buffer. */
   void *pixels = malloc(width * height * 4 * sizeof(float));
   glReadPixels(0, 0, width, height, GL_RGBA, GL_FLOAT, pixels);

   return 0;
}
