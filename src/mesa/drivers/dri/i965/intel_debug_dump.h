/*
 * Copyright 2015 Intel Corporation
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include <stdbool.h>

#include "intel_bufmgr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct brw_context;
struct intel_mipmap_tree;

bool
intel_debug_bo_dumps_enabled(void);

void
intel_debug_dump_bo(struct brw_context *brw,
                    drm_intel_bo *bo,
                    const char *basename);

void
intel_debug_dump_miptree(struct brw_context *brw,
                         struct intel_mipmap_tree *mt,
                         const char *basename);

void
intel_debug_memcpy_bo(struct brw_context *brw, drm_intel_bo *dest,
                      const void *data, size_t size);

void
intel_debug_dump_color_miptree(struct brw_context *brw,
                               struct intel_mipmap_tree *mt,
                               const char *basename);

void
intel_debug_dump_depth_miptree(struct brw_context *brw,
                               struct intel_mipmap_tree *mt,
                               const char *basename);

void
intel_debug_dump_framebuffer_color(struct brw_context *brw,
                                   const char *basename);

void
intel_debug_dump_framebuffer_depth(struct brw_context *brw,
                                   const char *basename);

#ifdef __cplusplus
}
#endif
