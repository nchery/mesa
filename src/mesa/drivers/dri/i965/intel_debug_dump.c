/*
 * Copyright 2015 Intel Corporation
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "intel_batchbuffer.h"
#include "intel_debug_dump.h"
#include "intel_fbo.h"
#include "intel_mipmap_tree.h"

#include "brw_state.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))

static _Atomic uint32_t intel_debug_dump_seq = 0;

bool
intel_debug_bo_dumps_enabled(void)
{
   static _Atomic int flag = -1;
   static const char *name = "INTEL_DEBUG_BO_DUMP";
   const char *env;

   if (flag != -1)
      return flag;

   env = getenv(name);
   if (env == NULL ||
       env[0] == '\0' ||
       strncmp(env, "0", 2) == 0) {
      flag = 0;
   } else if (strncmp(env, "1", 2) == 0) {
      flag = 1;
   } else {
      fprintf(stderr, "%s must be 0 or 1\n", name);
      abort();
   }

   return flag;
}

static void
dump_bo_mem(struct brw_context *brw, drm_intel_bo *bo,
            const char *basename, uint32_t seq)
{
   int ret;

   FILE *f;
   char filename[1024];

   if (drm_intel_bo_references(brw->batch.bo, bo))
      intel_batchbuffer_flush(brw);

   ret = snprintf(filename, sizeof(filename), "%04u-%p-%s.bin",
                  seq, bo, basename);
   if (ret < -1 || ret >= sizeof(filename))
      goto fail_basename_too_big;

   f = fopen(filename, "wx");
   if (!f) {
      if (errno == EEXIST)
         goto fail_file_exists;
      else
         goto fail_write;
   }

   ret = drm_intel_bo_map(bo, /*write_enable*/ false);
   if (ret == -1)
      goto fail_map;

   if (fwrite(bo->virtual, 1, bo->size, f) != bo->size)
      goto fail_write;

   drm_intel_bo_unmap(bo);

   if (fclose(f) == -1)
      goto fail_close;

   return;

 fail_basename_too_big:
   fprintf(stderr, "%s:%s: basename too big\n", __FILE__, __func__);
   abort();

 fail_file_exists:
   fprintf(stderr, "%s:%s: file already exists: %s\n", __FILE__, __func__, filename);
   abort();

 fail_write:
   fprintf(stderr, "%s:%s: failed to write file: %s\n", __FILE__, __func__, filename);
   abort();

 fail_map:
   fprintf(stderr, "%s:%s: failed to map bo\n", __FILE__, __func__);
   abort();

 fail_close:
   fprintf(stderr, "%s:%s: failed to close file: %s\n", __FILE__, __func__, filename);
   abort();
}

static void
dump_miptree_info(struct brw_context *brw,
                  struct intel_mipmap_tree *mt,
                  const char *basename, uint32_t seq)
{
   int ret;

   FILE *f;
   char filename[1024];

   ret = snprintf(filename, sizeof(filename), "%04u-%p-%s.info.txt",
                  seq, mt, basename);
   if (ret < -1 || ret >= sizeof(filename))
      goto fail_basename_too_big;

   f = fopen(filename, "wx");
   if (!f) {
      if (errno == EEXIST)
         goto fail_file_exists;
      else
         goto fail_write;
   }

   fprintf(f, "intel_mipmap_tree(%p):\n", mt);
   fprintf(f, "    brw_format: %u\n", brw_format_for_mesa_format(mt->format));
   fprintf(f, "    mesa_format: %u\n", mt->format);
   fprintf(f, "    mesa_format_name: %s\n", _mesa_get_format_name(mt->format));
   fprintf(f, "    cpp: %u\n", mt->cpp);
   fprintf(f, "    physical_width0: %u\n", mt->physical_width0);
   fprintf(f, "    physical_height0: %u\n", mt->physical_height0);
   fprintf(f, "    size: %u\n", mt->physical_height0 * mt->pitch);
   fprintf(f, "    pitch: %u\n", mt->pitch);

   if (fclose(f) == -1)
      goto fail_close;

   return;

 fail_basename_too_big:
   fprintf(stderr, "%s:%s: basename too big\n", __FILE__, __func__);
   abort();

 fail_file_exists:
   fprintf(stderr, "%s:%s: file already exists: %s\n", __FILE__, __func__, filename);
   abort();

 fail_write:
   fprintf(stderr, "%s:%s: failed to write file: %s\n", __FILE__, __func__, filename);
   abort();

 fail_close:
   fprintf(stderr, "%s:%s: failed to close file: %s\n", __FILE__, __func__, filename);
   abort();
}

void
intel_debug_dump_bo(struct brw_context *brw, drm_intel_bo *bo,
                    const char *basename)
{
   if (!intel_debug_bo_dumps_enabled())
      return;

   dump_bo_mem(brw, bo, basename, intel_debug_dump_seq++);
}

void
intel_debug_dump_miptree(struct brw_context *brw,
                         struct intel_mipmap_tree *mt,
                         const char *basename)
{
   if (!intel_debug_bo_dumps_enabled())
      return;

   uint32_t seq = intel_debug_dump_seq++;
   dump_miptree_info(brw, mt, basename, seq);
   dump_bo_mem(brw, mt->bo, basename, seq);
}

void
intel_debug_memcpy_bo(struct brw_context *brw, drm_intel_bo *dest,
                      const void *data, size_t size)
{
   int ret;

   ret = drm_intel_bo_map(dest, /*write_enable*/ true);
   if (ret == -1) {
      fprintf(stderr, "%s:%s: failed to map bo\n", __FILE__, __func__);
      abort();
   }

   memcpy(dest->virtual, data, MIN(size, dest->size));

   drm_intel_bo_unmap(dest);
}

static void UNUSED
example_overwrite_first_cacheline(struct brw_context *brw,
                                  struct intel_mipmap_tree *mt)
{
   /* Change this data to whatever you want then recompile. */
   const uint8_t cacheline[] = {
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
   };

   intel_debug_memcpy_bo(brw, mt->bo, cacheline, sizeof(cacheline));
}

void
intel_debug_dump_color_miptree(struct brw_context *brw,
                               struct intel_mipmap_tree *mt,
                               const char *basename)
{
   uint32_t seq = intel_debug_dump_seq++;
   char new_basename[1024];
   int ret;

   ret = snprintf(new_basename, sizeof(new_basename), "%s.color", basename);
   if (ret < -1 || ret >= sizeof(new_basename))
      goto fail_basename_too_big;

   dump_miptree_info(brw, mt, new_basename, seq);
   dump_bo_mem(brw, mt->bo, new_basename, seq);

   if (mt->mcs_mt) {
      ret = snprintf(new_basename, sizeof(new_basename), "%s.ccs", basename);
      if (ret < -1 || ret >= sizeof(new_basename))
         goto fail_basename_too_big;

      dump_miptree_info(brw, mt->mcs_mt, new_basename, seq);
      dump_bo_mem(brw, mt->mcs_mt->bo, new_basename, seq);
   }

   return;

 fail_basename_too_big:
   fprintf(stderr, "%s:%s: basename too big\n", __FILE__, __func__);
   abort();
}

void
intel_debug_dump_depth_miptree(struct brw_context *brw,
                               struct intel_mipmap_tree *mt,
                               const char *basename)
{
   uint32_t seq = intel_debug_dump_seq++;
   char new_basename[1024];
   int ret;

   ret = snprintf(new_basename, sizeof(new_basename), "%s.depth", basename);
   if (ret < -1 || ret >= sizeof(new_basename))
      goto fail_basename_too_big;

   dump_miptree_info(brw, mt, new_basename, seq);
   dump_bo_mem(brw, mt->bo, new_basename, seq);

   if (mt->hiz_buf) {
      ret = snprintf(new_basename, sizeof(new_basename), "%s.hiz", basename);
      if (ret < -1 || ret >= sizeof(new_basename))
         goto fail_basename_too_big;

      if (mt->hiz_buf->bo) {
         dump_bo_mem(brw, mt->hiz_buf->bo, new_basename, seq);
      } else if (mt->hiz_buf->mt) {
         dump_miptree_info(brw, mt->hiz_buf->mt, new_basename, seq);
         dump_bo_mem(brw, mt->hiz_buf->mt->bo, new_basename, seq);
      }
   }

   return;

 fail_basename_too_big:
   fprintf(stderr, "%s:%s: basename too big\n", __FILE__, __func__);
   abort();
}

void
intel_debug_dump_framebuffer_color(struct brw_context *brw,
                                   const char *basename)
{
   struct gl_context *ctx = &brw->ctx;

   struct gl_framebuffer *fb = ctx->DrawBuffer;
   if (!fb)
      return;

   const struct intel_renderbuffer *irb = intel_get_renderbuffer(fb, BUFFER_COLOR0);
   if (!irb)
      return;

   intel_debug_dump_color_miptree(brw, irb->mt, basename);
}

void
intel_debug_dump_framebuffer_depth(struct brw_context *brw,
                                   const char *basename)
{
   struct gl_context *ctx = &brw->ctx;

   struct gl_framebuffer *fb = ctx->DrawBuffer;
   if (!fb)
      return;

   const struct intel_renderbuffer *irb = intel_get_renderbuffer(fb, BUFFER_DEPTH);
   if (!irb)
      return;

   intel_debug_dump_depth_miptree(brw, irb->mt, basename);
}
